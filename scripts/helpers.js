// simple throttle function
// inspired by https://stackoverflow.com/questions/27078285/simple-throttle-in-js
export function throttle(callback, timeLimit) {
  let waiting = false
  return function(...args) {
    if (!waiting) {
      callback.apply(this, args)
      waiting = true
      setTimeout(() => {
        waiting = false
      }, timeLimit)
    }
  }
}

// inspired by https://gist.github.com/Daniel-Hug/abbded91dd55466e590b
export function delegate(element, event, descendentSelector, callback){
  element.addEventListener(event, function(e){
    const elem = e.target.closest(descendentSelector)
    // returns null if no matching parentNode is found
    if (elem) {
      callback.call(elem, e)
    }
  }, true);
}

export const getAdjustedTimingBasedOnDistance = (distance, maxDistance = 500, maxTime = 2000, minTime = 250) => {
  const ratio = Math.min(Math.abs(distance), maxDistance) / maxDistance
  return ratio * (maxTime - minTime) + minTime
}

export const createBoos = (container, count) => {
  return Array(count).fill().map(() => {
    const boo = document.createElement('div')
    const inner = document.createElement('div')
    boo.className = 'boo'
    inner.className = 'boo-inner'
    container.appendChild(boo)
    boo.appendChild(inner)
    return boo
  })
}
