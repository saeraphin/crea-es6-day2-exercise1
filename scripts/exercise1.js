import { throttle, createBoos, delegate, getAdjustedTimingBasedOnDistance } from './helpers.js'

(function exercise1() {
  /**
   * Ajoutons un peu d'animation à la scène.
   * 
   * 1. ajouter une animation au soleil
   *  a. animation infinie de votre choix (haut/bas, par exemple)
   *  b. ajouter une animation au visage du soleil (rotation, par exemple)
   * 
   * 2. ajouter une animation à la poussière (dust)
   *  a. déplacement horizontal complet (pour une boucle propre, utiliser une largeur d'écran horizontale 100vw)
   *  b. utiliser un event listener pour relancer l'animation quand elle est finie
   *  c. arrêter l'animation par défaut (utiliser playbackRate)
   *  d. régler la vitesse de lecture en fonction du ratio dans la fonction adjustDustSpeed
   * 
   * 3. animer les plans ground, mountains et mountainsFar avec un effet de paralax (les plans les plus éloignés bougent moins)
   *  a. utiliser de l'animation (but du cours)
   *  b. déplacer les plans horizontalement en fonction de la position de la souris
   *  c. les plans plus éloignés devraient moins bouger (diviser le déplacement par un facteur de votre choix, par exemple)
   *  d. le timing calculé est plus ou moins important en fonction de la distance, essayez de l'utiliser pour la durée de l'animation
   * 
   * 4. animer les boos
   *  a. déplacer les boos verticalement au passage de la souris
   *  b. déplacer les boos à la position verticale de la souris 
   * 
   * 
   * Bonus
   * 1. animer le container animation-ct en fonction d'un paramètre de votre choix
   *  a. essayer un peu de transformation 3d (rotateY)
   */



  // TODO #1.a b
  const sun = document.querySelector('#sun')



  // TODO #2.a b c
  const dust = document.querySelector('#dust')
  const dustAnimation = null

  const adjustDustSpeed = event => {
    const halfWidth = window.innerWidth / 2
    const relX = event.clientX - halfWidth
    const ratio = relX / halfWidth
    
    // TODO #2.d
  }



  // TODO #3
  const mountainsFar = document.querySelector('#mountains-far')
  const mountains = document.querySelector('#mountains')
  const ground = document.querySelector('#ground')

  let lastX = 0

  const animateGround = event => {
    const distance = lastX - event.clientX
    lastX = event.clientX
    const timing = getAdjustedTimingBasedOnDistance(distance)

    // TODO #3.a b c d
    // tip: use animation option fill: 'forwards'
  }



  // TODO #4
  const boosCt = document.querySelector('#boos')
  const boos = createBoos(boosCt, 10) // vous pouvez changer le nombre de boos ici

  delegate(boosCt, 'mouseenter', '.boo', function (event) {
    const boo = this
    const inner = boo.querySelector('.boo-inner')
    const bounds = boo.getBoundingClientRect()
    // y position relative to the parent container
    const relativeY = bounds.height - (event.clientY - bounds.top)

    // TODO #4
    // tip: animate the inner element
    // tip: use animation option fill: 'forwards'
  })

  delegate(boosCt, 'mouseleave', '.boo', function (event) {
    const inner = this.querySelector('.boo-inner')
    // TODO #4
    // tip: use animation option fill: 'forwards'
  })


  // SETUP
  // Add throttled listener on mouve move
  const one10thOfASecond = 1000 / 10
  window.addEventListener('mousemove', throttle((event) => {
    animateGround(event)
    adjustDustSpeed(event)
  }, one10thOfASecond)) // max calls : 10 times per second

})()